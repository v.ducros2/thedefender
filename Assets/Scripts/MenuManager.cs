using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private GameObject gameManager = null;
    // fonction pour bouton switch scene vers game
    public void StartGame()
    {
        SceneManager.LoadScene("Game");
    }

    // fonction pour bouton switch scene vers SkillBoard
    public void SkillBoard()
    {
        SceneManager.LoadScene("SkillBoard");
    }

    // fonction pour bouton switch scene vers LeaderBoard
    public void LeaderBoard()
    {
        SceneManager.LoadScene("LeaderBoard");
    }

    // fonction pour bouton quiter le jeu
    public void ExitGame()
    {
        Debug.Log("exitgame");
        Application.Quit();
    }

    public void ResumeGame()
    {
        gameManager.GetComponent<GameManager>().OnPauseToPlay();
    }

    //return to mainMenu & timeScale On
    public void MainMenu()
    {
        SceneManager.LoadScene("StartGame");
        Time.timeScale = 1;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }
}
