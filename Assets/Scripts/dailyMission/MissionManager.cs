using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class MissionManager : MonoBehaviour
{
    public GameObject[] listMission;
    public List<GameObject> listCurrentMission = new List<GameObject>();
    public GameObject missionNotAvailable = null;
    public static MissionManager currentMissionManager;

    private GameObject missionGameObjet = null;
    //private GameObject mission1GameObjet = null;
    //private GameObject mission2GameObjet = null;

    private int notValue = 999;
    private bool EventEnemyDiedToogle = false;
    private void Awake()
    {
        #region DontDestroy&DontDuplicate

        DontDestroyOnLoad(this.gameObject);
        if (currentMissionManager == null)
        {
            currentMissionManager = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        #endregion
    }
    // TODO 
    // quand les missions sont fini et que l'on relance le jeu pour recup les nouvelles mission
    // on a des probl�mes sur les valeur de currentNbKill
    // Start is called before the first frame update
    void Start()
    {
        #region GetCurrentMision&Value OrGetNewMission

        //recup�ration du num�ro de la mission 
        int typeOfMission1 = PlayerPrefs.GetInt("numberMission1", notValue);
        int typeOfMission2 = PlayerPrefs.GetInt("numberMission2", notValue);
        int typeOfMission3 = PlayerPrefs.GetInt("numberMission3", notValue);

        List<int> listTypeOfMission = new List<int>
        {
            typeOfMission1,
            typeOfMission2,
            typeOfMission3
        };

        // boucle pour gerer l'ajout de mission
        for (int i = 0; i < listTypeOfMission.Count; i++)
        {
            // l'index de la mission commence a 1
            int index = i + 1;
            // si la mission est deja cr�e
            if (listTypeOfMission[i] != notValue)
            {
                // on r�cup�re la mission li� a l'index dans la listeDesMissionsAvailable
                missionGameObjet = GetMission(listTypeOfMission[i]);
                // on r�cu�pre la valeur du nombre de kill de la mission
                int currentNbKill = PlayerPrefs.GetInt("valueMission"+ index, 0);
                // on check que la mission ne soit pas fini sinon on met la missionNotAvailable
                if (currentNbKill >= missionGameObjet.GetComponent<Mission>().nbKillMax)
                {
                    string stringTemp = PlayerPrefs.GetString("MissionEndDate" + index, "null");
                    if(stringTemp != "null")
                    {
                        // recup�ation du string convertion en int64 puis en DateTime
                        long temp = Convert.ToInt64(stringTemp);
                        DateTime oldDate = DateTime.FromBinary(temp);

                        //si on est le jour suivant la resolution de quete on recr�e une quete
                        if (oldDate.AddDays(1).Day <= DateTime.Now.Day)
                        {
                            missionGameObjet = InitMission(index);
                        }
                        //sinon NotAvailable
                        else
                        {
                            missionGameObjet = missionNotAvailable;
                        }
                    }//sinon NotAvailable
                    else
                    {
                        missionGameObjet = missionNotAvailable;

                    }
                }
                //si on reprend la mission en cours on SetNbCurrentkillByPlayerPref
                else
                {
                    // set nbCurrentkillByPlayerPref
                    missionGameObjet.GetComponent<Mission>().currentNbKill = currentNbKill;
                }
            }
            // si la mission n'existe pas
            else
            {
                missionGameObjet = InitMission(index);
            }

            // addMissionToCurrentMission
            listCurrentMission.Add(missionGameObjet);
        }

        #endregion
    }

    // Update is called once per frame
    void Update()
    {
        //add or remove lister if scene Game
        if (SceneManager.GetActiveScene().name == "Game")
        {
            AddlistenerOnEnemyDied();
        }
        else
        {
            RemovelistenerOnEnemyDied();
        }

    }

    // return sp�cifique mission
    private GameObject GetMission(int missionNumber)
    {
        return listMission[missionNumber];
    }

    // choice New Mission
    private GameObject InitMission(int index)
    {
        // choice random int mission
        int choice = UnityEngine.Random.Range(0, listMission.Length);
        // SetNumber mission
        PlayerPrefs.SetInt("numberMission" + index, choice);
        PlayerPrefs.SetInt("valueMission" + index, 0);

        //Debug.Log("numberMission" + PlayerPrefs.GetInt("numberMission" + index));
        //Debug.Log("playerprefValueMission" + PlayerPrefs.GetInt("valueMission" + index));

        // SetGameObjectMission
        GameObject initMissionGameObjet = listMission[choice];
        // SetCurrentNbKill
        initMissionGameObjet.GetComponent<Mission>().currentNbKill = 0;
        return initMissionGameObjet;
    }

    private void OnEnemyDied(ScriptableEnemyStat stat)
    {
        for (int i = 0; i < listCurrentMission.Count; i++)
        {
            Mission missionCheckObjectif = listCurrentMission[i].GetComponent<Mission>();
            // si la mission est de type Kill && si l'enemie mort fait bien partie de la quete
            if(missionCheckObjectif.typeMission == "Kill" && missionCheckObjectif.enemyToKill.nameEnemy == stat.nameEnemy)
            {
                //Debug.Log("enemyToKill = " + missionCheckObjectif.enemyToKill.nameEnemy);
                //Debug.Log("enemyKill = " + stat.nameEnemy);

                int indexMission = i + 1;
                //Debug.Log("indexMission = " + indexMission);
                // on update le nbKill
                missionCheckObjectif.currentNbKill += 1;
                //Debug.Log("nbkill = " + missionCheckObjectif.currentNbKill);
                PlayerPrefs.SetInt("valueMission"+ indexMission, missionCheckObjectif.currentNbKill);
                // on check si la mission est accomplie
                if (missionCheckObjectif.currentNbKill >= missionCheckObjectif.nbKillMax)
                {
                    //Debug.Log("endMission" + missionCheckObjectif.enemyToKill.nameEnemy);

                    PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money", 0)+ missionCheckObjectif.money);
                    PlayerPrefs.SetString("MissionEndDate"+ indexMission, System.DateTime.Now.ToBinary().ToString());
                    // set mission missionNotAvailable
                    listCurrentMission[i] = missionNotAvailable;
                }
            }
        }
    }

    #region Listener
    // fonction pour activer le listener dans la scene Game
    private void AddlistenerOnEnemyDied()
    {
        StartCoroutine(LaunchListener());
    }
    // fonction pour desactiver le listener dans les autres scene que game
    private void RemovelistenerOnEnemyDied()
    {
        if (EventEnemyDiedToogle == true)
        {
            Enemy.m_EnemyDied.RemoveListener(OnEnemyDied);
            EventEnemyDiedToogle = false;
        }
    }

    IEnumerator LaunchListener()
    {
        bool foundEnemy = false;

        while (foundEnemy == false)
        {
            yield return new WaitForSeconds(.1f);
            if (EventEnemyDiedToogle == false && FindObjectsOfType<Enemy>().Length > 0)
            {
                EventEnemyDiedToogle = true;
                Enemy.m_EnemyDied.AddListener(OnEnemyDied);
                foundEnemy = true;
            }
        }
    }
    #endregion

}
