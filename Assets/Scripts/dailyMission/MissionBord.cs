using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionBord : MonoBehaviour
{
    public List<GameObject> listCurrentMission = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        listCurrentMission = MissionManager.currentMissionManager.listCurrentMission;

        #region GetCurrentMision&Value OrGetNewMission

        foreach (GameObject missionGameObjet in listCurrentMission)
        {
            InstanciateMission(missionGameObjet);
        }

        #endregion
    }

    private void InstanciateMission(GameObject mision)
    {
        Instantiate(mision, this.transform);
    }
}
