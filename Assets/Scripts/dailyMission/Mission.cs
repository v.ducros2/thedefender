using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Mission : MonoBehaviour
{

    [SerializeField]
    public int nbKillMax = 0;
    [SerializeField]
    public int money = 0;
    [SerializeField]
    public ScriptableEnemyStat enemyToKill = null;
    [SerializeField]
    public GameObject moneyUI = null;
    [SerializeField]
    public GameObject objectifUI = null;
    [SerializeField]
    public string typeMission = null;

    public int currentNbKill = 0;
    //public GameObject missionManager = null;

    private void Start()
    {
        
    }

    private void Update()
    {
        if(typeMission == "Kill")
        {
            objectifUI.GetComponent<TextMeshProUGUI>().text = currentNbKill.ToString() + " / " + nbKillMax + " " + enemyToKill.nameEnemy;
            moneyUI.GetComponent<TextMeshProUGUI>().text = money.ToString();
        }
    }
}
