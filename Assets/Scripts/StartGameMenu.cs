using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StartGameMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject MoneyUi = null;

    // Update is called once per frame
    void Update()
    {
        MoneyUi.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetInt("money", 0).ToString();
    }

    [ContextMenu("give money plzz")]
    public void GiveMoney()
    {
        PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money", 0) + 1000);
    }
}
