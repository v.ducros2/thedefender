using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LeaderBoardManager : MonoBehaviour
{
    [SerializeField]
    GameObject PrefabScore = null;
    [SerializeField]
    GameObject LeaderBoardScoring = null;
    // Start is called before the first frame update
    void Start()
    {
        List<PlayerStat> listScore = SaveSysteme.LoadScore();

        if(listScore.Count > 0 && listScore != null)
        {
            foreach (PlayerStat playerStat in listScore)
            {
                GameObject scoreLine = Instantiate(PrefabScore, LeaderBoardScoring.transform);

                scoreLine.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = playerStat.Name;
                scoreLine.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = playerStat.Score.ToString();
                scoreLine.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = playerStat.TimePlayed.ToString();
            }
        }
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("StartGame");
    }

    [ContextMenu("ResetLeaderBoard")]
    public void ResetSaveGame()
    {
        SaveSysteme.ResetScore();
    }
}
