using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorGame : MonoBehaviour
{
    [SerializeField]
    Camera cam = null;
    [SerializeField]
    private GameObject gameManager = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // si on est en pause le tower ne peut pas faire d'action
        if (!gameManager.GetComponent<GameManager>().onPlay) return;

        Vector3 cursorGame = Input.mousePosition;
        cursorGame.z = 14f;

        this.transform.position = cam.ScreenToWorldPoint(cursorGame);

    }
}
