using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UITower : MonoBehaviour
{

    [SerializeField]
    private GameObject timerUI = null;
    [SerializeField]
    private GameObject scoreUI = null;
    [SerializeField]
    private GameObject lifeUI = null;
    [SerializeField]
    private GameObject cooldownUI = null;
    [SerializeField]
    public GameObject onPauseUI = null;
    [SerializeField]
    public GameObject onEndGameUI = null;

    public int rocketteType = 0;
    public int beamType = 0;
    public int supportType = 0;

    private GameObject cooldownRocketteUI;
    private GameObject cooldownBeamUI;
    private GameObject cooldownSupportUI;
    private void Start()
    {
        cooldownRocketteUI = SetRocketTypeUI(rocketteType);
        cooldownBeamUI = SetBeamTypeUI(beamType);
        cooldownSupportUI = SetSupportTypeUI(supportType);
    }

    // fonction pour modifier le timer dans l'UI
    public void SetTimerUI(float timer)
    {
        timerUI.transform.GetChild(0).GetComponent<TextMeshPro>().text = Math.Truncate(timer).ToString();
    }

    // fonction pour modifier le score dans l'UI
    public void SetScoreUI(int score)
    {
        scoreUI.transform.GetChild(0).GetComponent<TextMeshPro>().text = "score : " + score.ToString();
    }

    // fonction pour set la vie dans l'UI
    public void SetLifeUI(int currentLife, int maxLife)
    {
        if (currentLife <= 0)
        {
            currentLife = 0;
        }
        // set fillAmout of life bar around the tower
        lifeUI.transform.GetChild(0).GetComponent<Image>().fillAmount = (float) currentLife / maxLife;
    }

    #region setTypeAttaque

    // fonction pour setTypeRocketUI
    public GameObject SetRocketTypeUI(int rocketteType)
    {
        cooldownUI.transform.GetChild(0).GetChild(rocketteType).gameObject.SetActive(true);
        return cooldownUI.transform.GetChild(0).GetChild(rocketteType).gameObject;
    }

    // fonction pour setTypeLasertUI
    public GameObject SetBeamTypeUI(int beamType)
    {
        cooldownUI.transform.GetChild(1).GetChild(beamType).gameObject.SetActive(true);
        return cooldownUI.transform.GetChild(1).GetChild(beamType).gameObject;
    }

    public GameObject SetSupportTypeUI(int supportType)
    {
        cooldownUI.transform.GetChild(2).GetChild(supportType).gameObject.SetActive(true);
        return cooldownUI.transform.GetChild(2).GetChild(supportType).gameObject;
    }

    #endregion

    #region SetCooldown
    // fonction pour faire le volet sur le l'image pour affichier le cooldown de la rockette
    public void SetCooldownRocketteUI(float cooldown, float cooldownMax)
    {
        float amout = 0f;
        if (cooldown <= 0)
        {
            cooldown = 0;
        }
        // amount pour faire descendre le loader et le mettre a zero lorsque que le cooldown est good
        if(cooldown != 0)
        {
            amout = (float)1 - cooldown / cooldownMax;
        }
        else
        {
            amout = 0;
        }
        cooldownRocketteUI.transform.GetChild(1).GetComponent<Image>().fillAmount = amout;
    }

    // fonction pour faire le volet sur le l'image pour affichier le cooldown pour le beam
    public void SetCooldownBeamUI(float cooldown, float cooldownMax, int ammoBeamCurrent)
    {
        #region debug
        //Debug.Log("cooldown" + cooldown);
        //Debug.Log("cooldownMax" + cooldownMax);
        //Debug.Log("ammoBeamCurrent" + ammoBeamCurrent);
        #endregion

        //si on a plus d'une ammo on affiche pas la recharge

        if (ammoBeamCurrent < 1)
        {
            if (cooldown <= 0)
            {
                cooldown = 0;
            }

            cooldownBeamUI.transform.GetChild(1).GetComponent<Image>().fillAmount = (float)cooldown / cooldownMax;
        }
        else
        {
            cooldownBeamUI.transform.GetChild(1).GetComponent<Image>().fillAmount = 0;
        }

        // si il y a plus d'une ammo on set le compteur sinon on ne l'affiche pas
        if (ammoBeamCurrent > 1)
        {
            cooldownBeamUI.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = ammoBeamCurrent.ToString();
            
        }
        else
        {
            cooldownBeamUI.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "";
        }
    }

    // fonction pour faire le volet sur le l'image pour affichier le cooldown de support
    public void SetCooldownSupportUI(float cooldown, float cooldownMax)
    {
        float amout = 0f;
        if (cooldown <= 0)
        {
            cooldown = 0;
        }

        if (cooldown != 0)
        {
            amout = (float)1 - cooldown / cooldownMax;
        }
        else
        {
            amout = 0;
        }

        cooldownSupportUI.transform.GetChild(1).GetComponent<Image>().fillAmount = amout;
    }

    #endregion
}
