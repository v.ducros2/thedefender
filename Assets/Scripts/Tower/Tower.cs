using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField]
    private GameObject pointOfFire = null;
    [SerializeField]
    private GameObject bullet = null;
    [SerializeField]
    private GameObject rockette = null;
    [SerializeField]
    private GameObject rocketteArea = null;
    [SerializeField]
    private GameObject laserShort = null;
    [SerializeField]
    private GameObject laserLong = null;
    [SerializeField]
    private GameObject circleLaser = null;
    [SerializeField]
    private ParticleSystem laserParticule = null;
    [SerializeField]
    private GameObject cursorGame = null;
    [SerializeField]
    private GameObject camTower = null;
    [SerializeField]
    private GameObject gameManager = null;
    [SerializeField]
    private GameObject wallToPlace = null;
    [SerializeField]
    private ParticleSystem healParticule = null;

    #region setOnGameManager

    public int rocketteType = 0; 
    public int beamType = 0;
    public int supportType = 0;

    public int ammoBeamMax;
    public int ammoBeamCurrent;

    public int maxLife = 10;
    public int currentlife = 0;
    public bool ammoRockette = true;

    public bool ammoSupport = true;

    #endregion

    private GameObject laser;
    private int dommageLaser = 1;
    private float rangeBeam;
    private int amountHeal;

    private void Awake()
    {
        currentlife = maxLife;
    }

    // Start is called before the first frame update
    void Start()
    {
        SetSkillOnTower();
    }

    // Update is called once per frame
    void Update()
    {
        // si on est en pause le tower ne peut pas faire d'action
        if (!gameManager.GetComponent<GameManager>().onPlay) return;

        // rotation de la tourelle suit le crusor
        // on rend invisible le curseur en update parce que echap fait un cursor visible true
        Cursor.visible = false;
        RotatiobFollowCursor(cursorGame);


        // on mouse click fire
        if (Input.GetMouseButtonDown(0))
        {
            Fire();
        }
        if (Input.GetMouseButtonDown(1) && ammoBeamCurrent > 0)
        {
            
            if (beamType == 0)
            {
                FireLaser(pointOfFire);
            }
            if (beamType == 1)
            {
                FireCircleLaser();
            }
        }
        if (Input.GetMouseButtonDown(2) && ammoRockette == true)
        {
            if(rocketteType == 0)
            {
                FireRockette();
            }
            if(rocketteType == 1)
            {
                FireRocketteArea();
            }
            
        }
        if (Input.GetKeyDown(KeyCode.A) && ammoSupport == true)
        {
            
            if (supportType == 0)
            {
                FireHeal();
            }
            if (supportType == 1)
            {
                FireWallToPlace();
            }
        }

        
    }


    private void Fire()
    {
        GameObject projectile = (GameObject)Instantiate(bullet, this.pointOfFire.transform.position, this.pointOfFire.transform.rotation);
        projectile.GetComponent<Rigidbody>().AddForce(this.pointOfFire.transform.forward * 50, ForceMode.Impulse);
    }

    #region attaqueTypeRockette

    private void FireRockette()
    {
        ammoRockette = false;

        Vector3 pointDrop = new Vector3
        {
            x = this.cursorGame.transform.position.x,
            y = this.cursorGame.transform.position.y+10,
            z = this.cursorGame.transform.position.z
        };

        GameObject projectile = (GameObject)Instantiate(rockette, pointDrop, rockette.transform.rotation);
        projectile.GetComponent<Rockette>().camTower = camTower;
    }

    private void FireRocketteArea()
    {
        ammoRockette = false;

        Vector3 pointDrop = new Vector3
        {
            x = this.cursorGame.transform.position.x,
            y = 0,
            z = this.cursorGame.transform.position.z
        };

        GameObject projectile = (GameObject)Instantiate(rocketteArea, pointDrop, rockette.transform.rotation);
    }

    #endregion

    #region attaqueTypeBeam
    private void FireLaser(GameObject pointOfFire)
    {
        this.ammoBeamCurrent -= 1;

        StartCoroutine(InstanciateBeam(pointOfFire));
        RaycastHit[] hits;
        hits = Physics.RaycastAll(pointOfFire.transform.position,pointOfFire.transform.forward, rangeBeam);

        for (int i = 0; i < hits.Length; i++)
        {
            GameObject hit = hits[i].transform.gameObject;

            if (hit.CompareTag("Enemy"))
            {
                Instantiate(laserParticule,hit.transform.position,hit.transform.rotation);
                hit.GetComponent<Enemy>().TakeDommage(this.dommageLaser);
            }
        }
        
    }

    private void FireCircleLaser()
    {
        this.ammoBeamCurrent -= 1;
        
        Instantiate(circleLaser, circleLaser.transform.position, circleLaser.transform.rotation);

    }

    #endregion

    #region attaqueSupport

    private void FireHeal()
    {
        ammoSupport = false;

        currentlife += amountHeal;
        Instantiate(healParticule, this.transform.position, healParticule.transform.rotation);

        if (currentlife > maxLife)
        {
            currentlife = maxLife;
        }
    }

    private void FireWallToPlace()
    {
        ammoSupport = false;

        GameObject wall = Instantiate(wallToPlace, cursorGame.transform.position, Quaternion.identity);
        wall.GetComponent<WallToPlace>().cursorGame = this.cursorGame;
    }

    #endregion

    private IEnumerator InstanciateBeam(GameObject pointOfFire)
    {
        laser.SetActive(true);
        yield return new WaitForSeconds(.1f);
        laser.SetActive(false);
    }

    private void OnDrawGizmos()
    {
        // drowRedline for show range lazer
        Gizmos.color = Color.red;
        Vector3 direction = transform.TransformDirection(Vector3.forward) * 12;
        Gizmos.DrawRay(transform.position, direction);
    }

    private void RotatiobFollowCursor(GameObject cursorGame)
    {
        Vector3 targetDirection = cursorGame.transform.position - transform.position;
        targetDirection.y = transform.position.y;
        transform.LookAt(targetDirection);
    }

    public void TakeDomage(int domage)
    {
        this.currentlife -= domage;
    }

    private void SetSkillOnTower()
    {
        //SetUpRangeBeamBySkill
        int rangeBeamSkill = PlayerPrefs.GetInt("BeamRangeUpgrade", 0);
        if (rangeBeamSkill == 1)
        {
            rangeBeam = 12f;
            laser = laserLong;
        }
        else
        {
            rangeBeam = 7f;
            laser = laserShort;
        }
        //SetUpRangeBeamBySkill
        int MoreHeal = PlayerPrefs.GetInt("MoreHeal", 0);
        if (MoreHeal == 1)
        {
            amountHeal = 2;
        }
        else
        {
            amountHeal = 1;
        }
    }
}
