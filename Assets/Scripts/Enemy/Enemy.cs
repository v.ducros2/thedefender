using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;


[System.Serializable]
public class EnemyDiedEvent : UnityEvent<ScriptableEnemyStat>
{
    public ScriptableEnemyStat stat;
}

public class Enemy : MonoBehaviour
{
    public ScriptableEnemyStat stat = null;
    public GameObject target = null;
    public GameObject gameManager = null;

    private int life = 0;
    private int moveSpeed = 0;
    private int valueScore = 0;
    private Material materialColor = null;
    private NavMeshAgent myNavMeshAgent;

    public static EnemyDiedEvent m_EnemyDied;

    // Start is called before the first frame update
    void Start()
    {
        #region Eventdeath

        if (m_EnemyDied == null)
            m_EnemyDied = new EnemyDiedEvent();

        #endregion


        // take stats on ScriptableData
        life = stat.life;
        moveSpeed = stat.moveSpeed;
        valueScore = stat.score;
        materialColor = stat.materialColor;

        myNavMeshAgent = GetComponent<NavMeshAgent>();
        // set speed on navMesh
        myNavMeshAgent.speed = moveSpeed;
        this.GetComponent<MeshRenderer>().material = materialColor;

    }

    // Update is called once per frame
    void Update()
    {
        // set destination of the target
        myNavMeshAgent.SetDestination(target.transform.position);

        //check if is dead
        CheckCurrentLife();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Tower"))
        {
            collision.gameObject.GetComponent<Tower>().TakeDomage(1);
            Destroy(this.gameObject);
        }
    }

    public void TakeDommage (int dommage)
    {
        life -= dommage;
    }

    private void CheckCurrentLife()
    {
        if (life <= 0)
        {
            gameManager.GetComponent<GameManager>().AddScore(valueScore);
            // on re-set la valeur de stat a chaque fois qu'un enemy meurt 
            //ce qui permet d'avoir la valeur a jour lors des mission 
            m_EnemyDied.stat = stat;
            m_EnemyDied.Invoke(m_EnemyDied.stat);
            Destroy(this.gameObject);
        }
    }

}
