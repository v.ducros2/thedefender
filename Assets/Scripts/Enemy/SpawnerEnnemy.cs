using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnnemy : MonoBehaviour
{
    [SerializeField]
    GameObject enemyPrefab = null;
    [SerializeField]
    GameObject target = null;
    [SerializeField]
    GameObject gameManager = null;

    public ScriptableEnemyStat[] statsList;
    public GameObject[] spawnerListRegion;
    private List<GameObject> spawnerList = new List<GameObject>();

    private int rangeSpawn = 20;
    private float timeSpawn = 0.6f;

    //easy/medium/hard/blood&tears
    public int difficulty;

    // Start is called before the first frame update
    void Start()
    {
        SetDifficultyOnSpawner();
        StartCoroutine(InstanciateEnemy());
    }

    private IEnumerator InstanciateEnemy()
    {
        while (true)
        {
            Vector3 randomSphereSpawn = Random.insideUnitCircle * rangeSpawn;
            // select spawner
            randomSphereSpawn.y = 1;
            GameObject spawner = ChoiceSpawner();
            // instanciate enemy and position in cicleRandom on spawner
            GameObject enemy = Instantiate(enemyPrefab, spawner.transform.position + randomSphereSpawn, spawner.transform.rotation);
            // select stat and add to instantiate
            ScriptableEnemyStat stat = ChoiceStat();

            //set stat and target
            enemy.GetComponent<Enemy>().stat = stat;
            enemy.GetComponent<Enemy>().target = target;
            enemy.GetComponent<Enemy>().gameManager = gameManager;

            yield return new WaitForSeconds(timeSpawn);
        }

    }

    private void SetDifficultyOnSpawner()
    {
        /*
        spawnerListRegion[0] TOP
        spawnerListRegion[1] BOTTOM
        spawnerListRegion[2] LEFT
        spawnerListRegion[3] RIGHT
        */

        GameObject top = spawnerListRegion[0];
        GameObject bottom = spawnerListRegion[1];
        GameObject left = spawnerListRegion[2];
        GameObject right = spawnerListRegion[3];

        switch (difficulty)
        {
            //easy
            case 0:
                timeSpawn = 1.2f;
                AddRangeChildOfGameObject(top);
                break;
            //medium
            case 1:
                timeSpawn = 1f;
                AddRangeChildOfGameObject(top);
                AddRangeChildOfGameObject(bottom);
                break;
            //hard
            case 2:
                timeSpawn = 0.6f;
                AddRangeChildOfGameObject(top);
                AddRangeChildOfGameObject(bottom);
                AddRangeChildOfGameObject(left);
                AddRangeChildOfGameObject(right);
                break;
            //blood&tears
            case 3:
                timeSpawn = 0.4f;
                AddRangeChildOfGameObject(top);
                AddRangeChildOfGameObject(bottom);
                AddRangeChildOfGameObject(left);
                AddRangeChildOfGameObject(right);
                break;

        }
    }

    //add child gameObject of region to spawnerList
    private void AddRangeChildOfGameObject(GameObject regionObject)
    {
        int length = regionObject.transform.childCount;
        for (int i = 0; i < length; i++)
        {
            spawnerList.Add(regionObject.transform.GetChild(i).gameObject);
        }
    }

    // Random Choice of stats
    private ScriptableEnemyStat ChoiceStat()
    {
        int choice = Random.Range(0, statsList.Length);
        return statsList[choice];
    }

    // Random Choice of spawn
    private GameObject ChoiceSpawner()
    {
        int choice = Random.Range(0, spawnerList.Count);
        return spawnerList[choice];
    }
}
