using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/DataEnemy", order = 1)]
public class ScriptableEnemyStat : ScriptableObject
{
    public string nameEnemy;
    public int moveSpeed;
    public int life;
    public int score;
    public Material materialColor;
}
