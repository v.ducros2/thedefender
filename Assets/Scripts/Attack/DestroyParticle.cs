using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParticle : MonoBehaviour
{
    [SerializeField]
    private float timeToDestroy = 0 ;
    // destroy particule syteme before use
    void Start()
    {
        Destroy(this.gameObject, timeToDestroy);
    }
}
