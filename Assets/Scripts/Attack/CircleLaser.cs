using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleLaser : MonoBehaviour
{
    private Vector3 scaleChange;
    private int DommageCircleLaser;
    private float timeToDestroy;
    // Start is called before the first frame update
    void Start()
    {
        SetUpSkill();
        scaleChange = new Vector3(-0.01f, -0.01f, -0.01f);
        Destroy(this.gameObject, timeToDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        //scale down and after up laserArea
        transform.localScale += scaleChange;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            other.gameObject.GetComponent<Enemy>().TakeDommage(DommageCircleLaser);
        }
    }

    private void SetUpSkill()
    {
        //SetUp beamCircleNoSurvive Skill
        int beamCircleNoSurvive = PlayerPrefs.GetInt("BeamCircleNoSurvive", 0);
        if (beamCircleNoSurvive == 1)
        {
            DommageCircleLaser = 4;

        }
        else
        {
            DommageCircleLaser = 2;
        }

        //SetUp beamCircleNoSurvive Skill
        int beamCircleRangeUpgrade = PlayerPrefs.GetInt("BeamCircleRangeUpgrade", 0);
        if (beamCircleRangeUpgrade == 1)
        {
            timeToDestroy = 4f;

        }
        else
        {
            timeToDestroy = 2.5f;
        }
    }
}
