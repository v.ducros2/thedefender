using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketteCirlceBombe : MonoBehaviour
{
    [SerializeField]
    private GameObject explosionVFX = null;

    public int domageRockette = 1;
    private float radius ;
    // Start is called before the first frame update
    private void Start()
    {
        //setUp radius by skill RocketteAreaDamage
        int rocketteAreaDamageSkill = PlayerPrefs.GetInt("RocketteAreaDamage", 0);
        if(rocketteAreaDamageSkill == 1)
        {
            radius = 5;
        }
        else
        {
            radius = 3;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        GameObject explosion = Instantiate(explosionVFX, transform.position, Quaternion.identity);

        // radius dommages
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.CompareTag("Enemy"))
            {
                hitCollider.GetComponent<Enemy>().TakeDommage(domageRockette);
            }
        }

        Destroy(transform.gameObject);
    }
}
