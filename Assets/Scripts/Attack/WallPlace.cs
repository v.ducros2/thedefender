using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallPlace : MonoBehaviour
{
    private int timeToDestroy; 
    private void Start()
    {
        //SetUpRangeBeamBySkill
        int WallIncreaseLifeTime = PlayerPrefs.GetInt("WallIncreaseLifeTime", 0);
        if (WallIncreaseLifeTime == 1)
        {
            timeToDestroy = 10;
        }
        else
        {
            timeToDestroy = 5;
        }

        Destroy(this.gameObject, timeToDestroy);
    }
}
