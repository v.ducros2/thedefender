using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rockette : MonoBehaviour
{
    [SerializeField]
    private GameObject explosionVFX = null;
    [SerializeField]
    private GameObject camRockette = null;

    public GameObject camTower = null;
    public int domageRockette = 4;


    private float radius = 6;
    private int skillActivateCam;
    // Start is called before the first frame update
    void Start()
    {
        skillActivateCam = PlayerPrefs.GetInt("RocketteCam", 0);

        if(skillActivateCam == 1)
        {
            camTower.SetActive(false);
            camRockette.SetActive(true);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject explosion = Instantiate(explosionVFX, transform.position, Quaternion.identity);

        // radius dommages
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.CompareTag("Enemy"))
            {
                hitCollider.GetComponent<Enemy>().TakeDommage(domageRockette);
            }
        }

        Destroy(transform.gameObject);
    }

    private void OnDestroy()
    {
        if (skillActivateCam == 1)
        {
            camTower.SetActive(true);
            camRockette.SetActive(false);
        }
    }
}
