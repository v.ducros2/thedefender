using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleBombe : MonoBehaviour
{
    [SerializeField]
    private GameObject bombe = null;
    [SerializeField]
    private int rangeBombe = 5;
    private int nbBombe;
    private float instanciateTimeBombe;
    // Start is called before the first frame update
    void Start()
    {
        //setUp nbRockette & speed instanciante by skill RocketteAreaDoubleRockette
        int RocketteAreaDoubleRockette = PlayerPrefs.GetInt("RocketteAreaDoubleRockette", 0);
        if (RocketteAreaDoubleRockette == 1)
        {
            instanciateTimeBombe = .15f;
            nbBombe = 40;
        }
        else
        {
            instanciateTimeBombe = .3f;
            nbBombe = 20;
        }


        StartCoroutine(BombeOverTime());
    }

    IEnumerator BombeOverTime()
    {
        for (int i = 0; i < nbBombe; i++)
        {
            Vector3 randomCircle = Random.insideUnitCircle * rangeBombe;
            // hauteur de largage de bombe 50
            Vector3 positionSpawn = new Vector3(transform.position.x + randomCircle.x, 50, transform.position.z + randomCircle.y);
            Instantiate(bombe, positionSpawn, Quaternion.identity);
            yield return new WaitForSeconds(instanciateTimeBombe);
        }

        // on attend que les derni�res bombes tombes
        yield return new WaitForSeconds(3f);
        Destroy(this.gameObject);
    }
}
