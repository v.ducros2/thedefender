using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallToPlace : MonoBehaviour
{
    [SerializeField]
    private Material walltoPlaceGood = null;
    [SerializeField]
    private Material walltoPlaceNotGood = null;
    [SerializeField]
    private GameObject wallPlace = null;

    public GameObject cursorGame = null;
    private Color baseColor;

    private bool canInstanciateWall = true;
    private int angleRotationWall = 15;
    private Collider otherCheck;

    void Update()
    {
        SetPosition();
        SetRotation();
        InsanciateWall();
        CheckTriggerDestroy();
    }

    #region PositionWall
    private void SetPosition()
    {
        Vector3 cursorGamePosition = cursorGame.transform.position;
        cursorGamePosition.y = 0.65f;

        this.transform.position = cursorGamePosition;
    }

    private void SetRotation()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
            this.transform.Rotate(0, angleRotationWall, 0);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            this.transform.Rotate(0, -angleRotationWall, 0);
        }
    }
    #endregion

    #region WallCanInstancitate

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Tower"))
        {
            this.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = walltoPlaceGood;
            canInstanciateWall = true;
        }
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Tower"))
        {
            this.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = walltoPlaceNotGood;
            canInstanciateWall = false;
            otherCheck = other;
        }
    }

    //checkIf GameObject in trigger is destroy
    private void CheckTriggerDestroy()
    {
        if (canInstanciateWall == false && !otherCheck)
        {
            this.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = walltoPlaceGood;
            canInstanciateWall = true;
        }
    }

    #endregion

    private void InsanciateWall()
    {
        if (Input.GetMouseButtonDown(0) && canInstanciateWall == true)
        {
            Instantiate(wallPlace, this.transform.position, this.transform.rotation);
            Destroy(this.gameObject);
        }
    }
}
