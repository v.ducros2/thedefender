using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndGameScore : MonoBehaviour
{
    [SerializeField]
    private GameObject scoreUI = null;
    [SerializeField]
    private GameObject scoreCoefDifficultyUI = null;
    [SerializeField]
    private GameObject barreUI = null;
    [SerializeField]
    private GameObject scoreFinalUI = null;
    [SerializeField]
    private GameObject moneyEarnUI = null;
    [SerializeField]
    private GameObject inputUI = null;
    [SerializeField]
    private GameObject buttonUI = null;

    public int score;
    public int difficulty;
    public int timer;


    private float scoreCoefDifficulty;
    private float moneyCoefDifficulty;
    private int scoreFinal;
    private int moneyEarn;
    private string namePlayer;

    private bool checkValueInInput = false;

    private void Update()
    {
        CheckValueInInputSetButton();
    }

    private void CheckValueInInputSetButton()
    {
        if (string.IsNullOrEmpty(inputUI.GetComponent<TMP_InputField>().text))
        {
            checkValueInInput = false;
        }
        else
        {
            checkValueInInput = true;
            namePlayer = inputUI.GetComponent<TMP_InputField>().text;
        }

        if (checkValueInInput == true)
        {
            buttonUI.transform.GetComponent<Button>().interactable = true;
        }
        else
        {
            buttonUI.transform.GetComponent<Button>().interactable = false;
        }
    }

    public IEnumerator LaunchEndGame()
    {
        //computeScore&Money
        ComputeFinal();
        //setScore
        scoreUI.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = score.ToString();
        //setDifficulty
        scoreCoefDifficultyUI.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = scoreCoefDifficulty.ToString();
        //setScoreFinal
        scoreFinalUI.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = scoreFinal.ToString();
        //setMoney
        moneyEarnUI.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = moneyEarn.ToString();

        GameObject[] listScoreBoard = new GameObject[] { scoreUI, scoreCoefDifficultyUI, barreUI, scoreFinalUI, moneyEarnUI, inputUI , buttonUI };
        foreach (GameObject gameObjectUI in listScoreBoard)
        {
            yield return new WaitForSeconds(.3f);
            gameObjectUI.SetActive(true);
        }
        // timeScale Off now pour arrete le jeu mais que l'affichage ce fasse en cascade
        Time.timeScale = 0;
    }

    private void ComputeFinal()
    {
        switch (difficulty)
        {
            case 0:
                scoreCoefDifficulty = 0.5f;
                moneyCoefDifficulty = 5;
                break;
            case 1:
                scoreCoefDifficulty = 0.75f;
                moneyCoefDifficulty = 3;
                break;
            case 2:
                scoreCoefDifficulty = 1f;
                moneyCoefDifficulty = 1;
                break;
            case 3:
                scoreCoefDifficulty = 1.5f;
                moneyCoefDifficulty = 0.65f;
                break;     
        }

        scoreFinal = (int)(score * scoreCoefDifficulty);
        moneyEarn = (int)(score / moneyCoefDifficulty);
        PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money", 0) + moneyEarn);
    }

    // OnClick SaveScore & Switch scene
    public void SaveGameGoToLeaderboard()
    {
        // save score
        SaveSysteme.SaveScore(namePlayer, scoreFinal, (int)timer);
        // switch Scene
        SceneManager.LoadScene("LeaderBoard");
    }

}
