using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SkillBoardManager : MonoBehaviour
{
    [SerializeField]
    private GameObject MoneyUi = null;
    [SerializeField]
    private GameObject difficultyUI = null;

    private Color colorNotSelect = Color.gray;

    // Update is called once per frame
    void Update()
    {
        MoneyUi.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetInt("money", 0).ToString();
    }

}
