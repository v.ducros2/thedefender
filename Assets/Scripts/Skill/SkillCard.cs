using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SkillCard : MonoBehaviour
{
    public bool cardActive;
    public bool skillIsLock;

    [SerializeField]
    private ScriptableSkill dataSkill = null;

    // setByScriptable
    public int cardPrice;
    public bool skillHaveLock;
    public string nameSkill;
    public string nameSkillLock;
    public int skillValueSet;
    public int skillRevertValueSet;

    private Color colorNotSelect = Color.gray;
    private Color colorSelect = Color.white;

    public virtual void Start()
    {
        //take data from scriptable
        cardPrice = dataSkill.cardPrice;
        skillHaveLock = dataSkill.skillHaveLock;
        nameSkill = dataSkill.nameSkill;
        nameSkillLock = dataSkill.nameSkillLock;
        skillValueSet = dataSkill.skillValueSet;
        skillRevertValueSet = dataSkill.skillRevertValueSet;

        SetPriceCard();
        if(skillHaveLock == true)
        {
            skillIsLock = CheckLockCard(nameSkillLock);
        }
    }
    // Update is called once per frame
    public virtual void Update()
    {
        SetColorCardActive();
        CheckCardActive(nameSkill, skillValueSet);
        SetCardLock();
    }

    //SetColor Select on Card
    private void SetColorCardActive()
    {
        if(cardActive == false)
        {
            transform.GetChild(0).GetComponent<Image>().color = colorNotSelect;
        }
        else
        {
            transform.GetChild(0).GetComponent<Image>().color = colorSelect;
        }
    }

    //SetUp image lock on card if lock
    private void SetCardLock()
    {
        if (skillHaveLock == true && skillIsLock == false)
        {
            transform.GetChild(1).gameObject.SetActive(false);
        }
        else if (skillHaveLock == true && skillIsLock == true)
        {
            transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    // Set pirce on card
    private void SetPriceCard()
    {
        //si le prix est different de zero on est pas sur un defaultCard on setPrice
        if (cardPrice != 0)
        {
            transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = cardPrice.ToString();
        }
    }

    // on Button
    public void ClickOnCard()
    {
        //try to buy
        if (skillHaveLock == true && skillIsLock == true)
        {
            PaySkill(nameSkillLock);
        }
        // si il n'a pas lock ou si il y a un lock et qu'il est unlock
        if(skillHaveLock == false || (skillHaveLock == true && skillIsLock == false))
        {
            if (cardActive == false)
            {
                OnClickAction(nameSkill, skillValueSet);

            }
            else
            {
                OnClickRevertAction(nameSkill, skillRevertValueSet);
            }
        }        
    }

    // checkCardIsSelect with value of playerPref is the value of the skill
    public void CheckCardActive(string nameSkill, int skillValueSet)
    {
        if (PlayerPrefs.GetInt(nameSkill, 0) == skillValueSet)
        {
            this.cardActive = true;
        }
        else
        {
            this.cardActive = false;
        }
    }

    // buy skill > take money > SetUnlock
    public void PaySkill(string nameSkillLock)
    {
        int money = PlayerPrefs.GetInt("money", 0);
        if (money >= cardPrice)
        {
            PlayerPrefs.SetInt(nameSkillLock, 1);
            PlayerPrefs.SetInt("money", money - cardPrice);
            skillIsLock = false;
        }
    }

    // setUp skill to active with skillValueSet
    public void OnClickAction(string nameSkill,int skillValueSet)
    {
        PlayerPrefs.SetInt(nameSkill, skillValueSet);
    }

    public void OnClickRevertAction(string nameSkill, int skillRevertValueSet)
    {
        PlayerPrefs.SetInt(nameSkill, skillRevertValueSet);
    }

    // checkPlayerPrefAlreadyBuy
    public bool CheckLockCard(string nameSkillLock)
    {
        int isLock = PlayerPrefs.GetInt(nameSkillLock, 0);

        if (isLock == 1)
        {
            return false;
        }
        return true;
    }
}
