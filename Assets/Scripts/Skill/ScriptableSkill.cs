using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/DataSkill", order = 1)]

public class ScriptableSkill : ScriptableObject
{
    public int cardPrice;
    public bool skillHaveLock;
    public string nameSkill;
    public string nameSkillLock;
    public int skillValueSet;
    public int skillRevertValueSet;
}
