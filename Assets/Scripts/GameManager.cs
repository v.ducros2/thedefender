using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    GameObject towerUI = null;
    [SerializeField]
    GameObject tower = null;
    [SerializeField]
    GameObject spawnerEnnemy = null;

    

    public float timer = 0;
    public float timerCooldownRockette = 0;
    public float timerCooldownRocketteMax = 0;
    public float timerCooldownBeam = 0;
    public float timerCooldownBeamMax = 0;
    public float timerCooldownSupport = 0;
    public float timerCooldownSupportTypeMax = 0;
    public GameObject endGameUI;

    public int score = 0;
    public bool onPlay = true;
    //easy/medium/hard/blood&tears
    public int difficulty = 0; 

    //init
    private bool ammoRockette = true;
    private bool ammoSupport = true;
    private int ammoBeamMax;
    private int ammoBeamCurrent;
    private int maxLifeTower = 0;
    private int currentLifeTower = 0;
    private bool isEndGame = false;

    private void Awake()
    {
        // on set la difficulte
        difficulty = SetDifficulty();
        endGameUI = towerUI.GetComponent<UITower>().onEndGameUI;
        // on set les competence active
        SetRocketteTypeOnTowerTowerUI();
        SetBeamTypeOnTowerTowerUI();
        SetSupportTypeOnTowerTowerUI();
    }

    // Start is called before the first frame update
    void Start()
    {
        maxLifeTower = tower.GetComponent<Tower>().maxLife;
    }

    // Update is called once per frame
    void Update()
    {
        // GetValue in tower
        currentLifeTower = tower.GetComponent<Tower>().currentlife;
        ammoRockette = tower.GetComponent<Tower>().ammoRockette;
        ammoBeamCurrent = tower.GetComponent<Tower>().ammoBeamCurrent;
        ammoSupport = tower.GetComponent<Tower>().ammoSupport;

        // update time & time cooldown
        timer += Time.deltaTime;
        TimerCooldownRockette();
        TimerCooldownBeam();
        TimerCooldownSupport();

        // MenuPause
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (onPlay)
                OnPlayToPause();
            else
                OnPauseToPlay();
        }

        #region UpdateUI
        //update timer
        towerUI.GetComponent<UITower>().SetTimerUI(timer);
        //update lifeBarreUI
        towerUI.GetComponent<UITower>().SetLifeUI(currentLifeTower, maxLifeTower);
        //update cooldownRockette
        towerUI.GetComponent<UITower>().SetCooldownRocketteUI(timerCooldownRockette, timerCooldownRocketteMax);
        //update cooldownBeam
        towerUI.GetComponent<UITower>().SetCooldownBeamUI(timerCooldownBeam, timerCooldownBeamMax, ammoBeamCurrent);
        #endregion
        towerUI.GetComponent<UITower>().SetCooldownSupportUI(timerCooldownSupport,timerCooldownSupportTypeMax);
        // checkEndGame
        EndGame(currentLifeTower);
    }

    // fonction for update scoreUI and local score
    public void AddScore(int scoreSup)
    {
        score += scoreSup;
        towerUI.GetComponent<UITower>().SetScoreUI(score);
    }

    #region Function in Update

    // when tower is dead
    private void EndGame(int currentLife)
    {
        //switch scene leaderBord
        if (currentLife == 0 && isEndGame == false)
        {
            Cursor.visible = true;
            isEndGame = true;

            endGameUI.SetActive(true);
            endGameUI.GetComponent<EndGameScore>().score = score;
            endGameUI.GetComponent<EndGameScore>().difficulty = difficulty;
            endGameUI.GetComponent<EndGameScore>().timer = (int)timer;

            StartCoroutine(endGameUI.GetComponent<EndGameScore>().LaunchEndGame());
            onPlay = !onPlay;
        }
    }

    #region TimerCoolDownAttaque
    private void TimerCooldownRockette()
    {
        if (ammoRockette == false)
        {
            timerCooldownRockette += Time.deltaTime;
            if(timerCooldownRockette>= timerCooldownRocketteMax)
            {
                tower.GetComponent<Tower>().ammoRockette = true;
                timerCooldownRockette = 0f;
            }
        }
        else
        {
            timerCooldownRockette = 0f;
        }
    }

    private void TimerCooldownBeam()
    {

        if (ammoBeamCurrent < ammoBeamMax)
        {
            timerCooldownBeam += Time.deltaTime;
            if (timerCooldownBeam >= timerCooldownBeamMax)
            {
                tower.GetComponent<Tower>().ammoBeamCurrent +=1;
                timerCooldownBeam = 0;
            }
        }
        else
        {
            timerCooldownBeam = 0f;
        }
    }

    private void TimerCooldownSupport()
    {
        if (ammoSupport == false)
        {
            timerCooldownSupport += Time.deltaTime;
            if (timerCooldownSupport >= timerCooldownSupportTypeMax)
            {
                tower.GetComponent<Tower>().ammoSupport = true;
                timerCooldownSupport = 0f;
            }
        }
        else
        {
            timerCooldownSupport = 0f;
        }
    }

    #endregion

    #region SetAttaqueType
    private void SetRocketteTypeOnTowerTowerUI()
    {
        int rocketteType = PlayerPrefs.GetInt("RocketteType", 0);
        if (rocketteType == 0)
        {
            // SetUp cooldownRockette by skill
            int timerCooldownRocketteSkill = PlayerPrefs.GetInt("timerCooldownRockette", 0);
            if(timerCooldownRocketteSkill == 1)
            {
                timerCooldownRocketteMax = 7f;
            }
            else
            {
                timerCooldownRocketteMax = 10f;
            }
            
        }
        if (rocketteType == 1)
        {
            timerCooldownRocketteMax = 15f;
        }

        // set rockette type on tower and towerUI
        tower.GetComponent<Tower>().rocketteType = rocketteType;
        towerUI.GetComponent<UITower>().rocketteType = rocketteType;
    }

    private void SetBeamTypeOnTowerTowerUI()
    {
        int beamType = PlayerPrefs.GetInt("BeamType", 0);
        if (beamType == 0)
        {
            // SetUp ammoBeamMax by skill
            int beamMoreAmmoSkill = PlayerPrefs.GetInt("BeamMoreAmmo", 0);
            if (beamMoreAmmoSkill == 1)
            {
                ammoBeamMax = 8;
            }
            else
            {
                ammoBeamMax = 5;
            }

            timerCooldownBeamMax = 5f;
            
        }
        if (beamType == 1)
        {
            timerCooldownBeamMax = 15f;
            ammoBeamMax = 1;
        }

        // set rockette type on tower and towerUI
        tower.GetComponent<Tower>().beamType = beamType;
        towerUI.GetComponent<UITower>().beamType = beamType;
        tower.GetComponent<Tower>().ammoBeamMax = ammoBeamMax;
        tower.GetComponent<Tower>().ammoBeamCurrent = ammoBeamMax;
    }

    private void SetSupportTypeOnTowerTowerUI()
    {
        int supportType = PlayerPrefs.GetInt("SupportType", 0);
        if (supportType == 0)
        {
            // SetUp timerCooldownSupportTypeMax by skill HealReduceCooldown
            int HealReduceCooldown = PlayerPrefs.GetInt("HealReduceCooldown", 0);
            if (HealReduceCooldown == 1)
            {
                timerCooldownSupportTypeMax = 7f;
            }
            else
            {
                timerCooldownSupportTypeMax = 10f;
            }
        }
        if (supportType == 1)
        {
            // SetUp timerCooldownSupportTypeMax by skill WallReduceCooldown
            int WallReduceCooldown = PlayerPrefs.GetInt("WallReduceCooldown", 0);
            if (WallReduceCooldown == 1)
            {
                timerCooldownSupportTypeMax = 6f;
            }
            else
            {
                timerCooldownSupportTypeMax = 15f;
            }
        }

        // set rockette type on tower and towerUI
        tower.GetComponent<Tower>().supportType = supportType;
        towerUI.GetComponent<UITower>().supportType = supportType;
    }

    #endregion

    private int SetDifficulty()
    {
        int difficultyPlayerPref = PlayerPrefs.GetInt("difficulty", 0);
        spawnerEnnemy.GetComponent<SpawnerEnnemy>().difficulty = difficultyPlayerPref;
        return difficultyPlayerPref;
    }


    public void OnPlayToPause()
    {
        Cursor.visible = true;
        Time.timeScale = 0;
        towerUI.GetComponent<UITower>().onPauseUI.SetActive(true);
        onPlay = !onPlay;
    }

    public void OnPauseToPlay()
    {
        Cursor.visible = false;
        Time.timeScale = 1;
        towerUI.GetComponent<UITower>().onPauseUI.SetActive(false);
        onPlay = !onPlay;
    }

    #endregion
}
