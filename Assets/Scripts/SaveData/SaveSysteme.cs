using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;

public class SaveSysteme
{
    public static string pathSaveFile = Application.persistentDataPath + "/leaderBord.game";
    public static void SaveScore(string name, int score, int timePlayed)
    {
        // on initialise le formater en binary & on cr�e un playerStat
        BinaryFormatter formatter = new BinaryFormatter();
        PlayerStat playerStat = new PlayerStat(name, score, timePlayed);

        // si le fichier existe deja on ajoute a l'existant
        if (File.Exists(pathSaveFile))
        {
            // on ouvre le stream du fichier on recup�re le tableau on referme le stream
            FileStream stream = new FileStream(pathSaveFile, FileMode.Open);
            List<PlayerStat> data = formatter.Deserialize(stream) as List<PlayerStat>;
            stream.Close();

            // ouverture d'un stream en create pour surcharger le fichier
            FileStream streamCreate = new FileStream(pathSaveFile, FileMode.Create);
            data = OrderByScore(data);
            // si le tableau est deja au max de score on enleve le score le plus faible
            if (data.Count == 10)
            {
                data.RemoveAt(9);
            }

            // on complete le tableau que l'on reserialize
            data.Add(playerStat);
            formatter.Serialize(streamCreate, data);
            streamCreate.Close();
        }
        else
        {
            // si le fichier n'existe pas on le cr�e et on ajoute la list avec le playerStat
            FileStream stream = new FileStream(pathSaveFile, FileMode.Create);
            
            List<PlayerStat> data = new List<PlayerStat>
            {
                playerStat
            };
            formatter.Serialize(stream, data);
            stream.Close();
        }
    }

    public static List<PlayerStat> LoadScore()
    {

        if (File.Exists(pathSaveFile))
        {
            // on initialise le formater en binary & on cr�e le stream
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(pathSaveFile, FileMode.Open);

            // on recup�re la list
            List<PlayerStat> data = formatter.Deserialize(stream) as List<PlayerStat>;
            stream.Close();

            // order by score
            data = OrderByScore(data);
            return data;
        }
        else
        {
            Debug.Log("Save File Not found in " + pathSaveFile);
            return null;

        }
    }

    public static void ResetScore()
    {
        if (File.Exists(pathSaveFile))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(pathSaveFile, FileMode.Create);
            List<PlayerStat> data = new List<PlayerStat>();

            formatter.Serialize(stream, data);
            stream.Close();
        }
        else
        {
            Debug.Log("Save File Not found in " + pathSaveFile);
        }
    }

    private static List<PlayerStat> OrderByScore(List<PlayerStat> listPlayerStats)
    {
        List<PlayerStat> returnList = new List<PlayerStat>();

        var numQuery =
            from playerStat in listPlayerStats
            orderby playerStat.Score descending
            select playerStat;

        returnList = numQuery.ToList();

        return returnList;
    }
}
