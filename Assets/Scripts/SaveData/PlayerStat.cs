using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerStat
{
    public string Name;
    public int Score;
    public int TimePlayed;

    public PlayerStat(string name, int score, int timePlayed)
    {
        Name = name;
        Score = score;
        TimePlayed = timePlayed;
    }
}
